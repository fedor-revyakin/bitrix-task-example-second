<?php
if (! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}

$arComponentDescription = [
    "NAME" => "Мой компонент",
    "PATH" => [
        "ID" => "exam",
        "NAME" => "Экзамен 2"
    ],
];
