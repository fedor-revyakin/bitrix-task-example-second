<?php
if (! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

/** @var $arResult */
?>
<?if (! $arResult["ITEMS"]):?>
    <h3>
        Нет товаров, связанных с фирмой - производителем
    </h3>
<?else:?>
    <div>
        <ul>
            <?foreach ($arResult["ITEMS"] as $item):?>
            <li>
                <?="{$item["NAME"]}"?>
                <ul>
                    <?foreach ($item["PRODUCTS"] as $product):?>
                        <li>
                            <a href="<?=$product["DETAIL_PAGE_URL"]?>">
                                <?="{$product["NAME"]} - {$product["PRICE"]} - {$product["MATERIAL"]} - ({$product["DETAIL_PAGE_URL"]})"?>
                            </a>
                        </li>
                    <?endforeach;?>
                </ul>
            </li>
            <?endforeach;?>
        </ul>
    </div>
<?endif;?>