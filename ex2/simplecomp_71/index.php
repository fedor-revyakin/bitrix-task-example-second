<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

global $APPLICATION;

$APPLICATION->IncludeComponent("simplecomp_71.exam", ".default",
    [
        "PRODUCTS_IBLOCK_ID" => PRODUCTS_IBLOCK_ID,
        "COMPANY_IBLOCK_ID" => COMPANY_IBLOCK_ID,
        "LINK_PROPERTY_CODE" => "COMPANY",
        "DETAIL_URL_TEMPLATE" => "/catalog_exam/#SECTION_ID#/#ELEMENT_CODE#/",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36000000"
    ],
);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");