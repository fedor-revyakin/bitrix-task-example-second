<?php
if (! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

/** @var $arParams */
/** @var $arResult */
/** @var $APPLICATION */
/** @var $this CBitrixComponentTemplate */
?>

<?if (! $arResult["ITEMS"]):?>
    <h3>
        Нет товаров, связанных с новостями
    </h3>
<?else:?>
    <div id="<?=$this->GetEditAreaID($arParams["PRODUCTS_IBLOCK_ID"])?>">
        <a href="?F=Y">
            Фильтр: <?="{$APPLICATION->GetCurPage()}?F=Y"?>
        </a>
        <ul>
            <?foreach ($arResult["ITEMS"] as $item):?>
            <li>
                <?="{$item["NEWS"]["NAME"]} - {$item["NEWS"]["DATE"]} ({$item["SECTION_NAME"]})"?>
                <ul>
                    <?foreach ($item["PRODUCTS"] as $product):?>
                        <li id="<?=$this->GetEditAreaId($product["ID"]);?>">
                            <?="{$product["NAME"]} - {$product["PRICE"]} - {$product["MATERIAL"]} - {$product["ARTICLE"]}"?>
                        </li>
                    <?endforeach;?>
                </ul>
            </li>
            <?endforeach;?>
        </ul>
    </div>
<?endif;?>