<?php
if (! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

/** @var $APPLICATION */
/** @var $arParams */
/** @var $arResult */

if (($iblockId = intval($arParams["CANONICAL_IBLOCK_ID"])) && ($id = $arResult["ID"])) {
    $rsElem = CIBlockElement::GetList(
        [],
        [
            "IBLOCK_ID" => $iblockId,
            "PROPERTY_NEWS.ID" => $id,
        ],
        false,
        [
            "nTopCount" => 1,
        ],
        [
            "NAME"
        ]
    );

    if ($name = $rsElem->GetNext()["NAME"]) {
        $APPLICATION->SetPageProperty(CANONICAL_PROPERTY_NAME, $name);
    }
}
