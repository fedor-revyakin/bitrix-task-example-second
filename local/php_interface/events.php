<?php

//[ex2-50] Проверка при деактивации товара
AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", ["IBlockHandler", "OnBeforeIBlockElementUpdateHandler"]);

//[ex2-93] Записывать в Журнал событий открытие не существующих страниц сайта
AddEventHandler("main", "OnAfterEpilog", ["PageHandler", "LogNonExistentPage"]);

//[ex2-51] Изменение данных в письме
AddEventHandler('main', 'OnBeforeEventAdd', ["MailEventHandler", "OnBeforeEventAddHandler"]);

//[ex2-94] Супер инструмент SEO специалиста
AddEventHandler("main", "OnEpilog", ["PageHandler", "ReplaceMetaTag"]);

class IBlockHandler
{
    const SHOW_COUNTER_MAX = 2;

    static function OnBeforeIBlockElementUpdateHandler(&$arFields)
    {
        if ($arFields["IBLOCK_ID"] == PRODUCTS_IBLOCK_ID) {
            $currentFields = self::getCurrentFields($arFields["ID"], ["ACTIVE", "SHOW_COUNTER"]);
            if ($currentFields["ACTIVE"] == "Y" && $arFields["ACTIVE"] != "Y" && ($count = intval($currentFields["SHOW_COUNTER"])) > self::SHOW_COUNTER_MAX) {
                global $APPLICATION;
                $APPLICATION->ThrowException("Товар невозможно деактивировать, у него {$count} просмотров");
                return false;
            }
        }
    }

    private static function getCurrentFields(int $id, array $arSelect): array
    {
        return CIBlockElement::GetList(
            [],
            ["ID" => $id],
            false,
            false,
            $arSelect,
        )->GetNext() ?? [];
    }
}

class PageHandler
{
    static function LogNonExistentPage(): void
    {
        if (defined("ERROR_404")) {
            global $APPLICATION;
            CEventLog::Add(
                [
                    "SEVERITY" => "INFO",
                    "AUDIT_TYPE_ID" => "ERROR_404",
                    "MODULE_ID" => "main",
                    "DESCRIPTION" => $APPLICATION->GetCurPage(),
                ]
            );
        }
    }

    static function ReplaceMetaTag(): void
    {
        global $APPLICATION;
        $tags = getCustomMetaTags()[$APPLICATION->GetCurPage()];
        foreach ($tags as $name => $value) {
            if ($value) {
                $APPLICATION->SetPageProperty($name, $value);
            }
        }
    }
}

class MailEventHandler
{
    static function OnBeforeEventAddHandler(string &$event, string &$lid, array &$arFields): void
    {
        if ($event == FEEDBACK_FORM_EVENT_TYPE) {
            global $USER;
            if ($USER->IsAuthorized()) {
                $arFields["AUTHOR"] = "Пользователь авторизован: {$USER->GetID()} ({$USER->GetLogin()}) {$USER->GetFullName()}, данные из формы: {$arFields["AUTHOR"]}";
            } else {
                $arFields["AUTHOR"] = "Пользователь не авторизован, данные из формы: {$arFields["AUTHOR"]}";
            }
        }
    }
}
