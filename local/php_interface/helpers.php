<?php

use Bitrix\Main\GroupTable;
use Bitrix\Main\Loader;

function groupCodes(): array
{
    $cache = new CPHPCache();
    if ($cache->InitCache("36000000", "groupCodes")) {
        return $cache->GetVars();
    } else {
        $cache->StartDataCache();
        $groupCodes = [];
        foreach (GroupTable::getList(["select" => ["ID", "STRING_ID"]])->fetchAll() as $elem) {
            $groupCodes[$elem["STRING_ID"]] = $elem["ID"];
        }
        if ($groupCodes) {
            $cache->EndDataCache($groupCodes);
        } else {
            $cache->AbortDataCache();
        }

        return $groupCodes;
    }
}

function getCustomMetaTags(): array
{
    $cache = new CPHPCache();
    if ($cache->InitCache(36000000, "customMetaTags")) {
        return $cache->GetVars();
    } else {

        Loader::includeModule("iblock");

        $cache->StartDataCache();

        $rsTags = CIBlockElement::GetList(
            [],
            [
                "IBLOCK_ID" => METATAGS_IBLOCK_ID,
            ],
            false,
            false,
            [
                "ID",
                "IBLOCK_ID",
                "NAME",
                "PROPERTY_TITLE",
                "PROPERTY_DESCRIPTION",
            ]
        );

        $tags = [];
        while ($elem = $rsTags->GetNext()) {
            $tags[$elem["NAME"]] = [
                "title" => $elem["PROPERTY_TITLE_VALUE"],
                "description" => $elem["PROPERTY_DESCRIPTION_VALUE"],
            ];
        }

        if ($tags) {
            $cache->EndDataCache($tags);
        } else {
            $cache->AbortDataCache();
        }

        return $tags;
    }
}
