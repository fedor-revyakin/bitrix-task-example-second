<?php
if (! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

/** @var array $arResult */
/** @global CMain $APPLICATION */

use Bitrix\Main\Loader;

if (! isset($arParams["CACHE_TIME"])) {
    $arParams["CACHE_TIME"] = 36000000;
}

$arParams["PRODUCTS_IBLOCK_ID"] = intval($arParams["PRODUCTS_IBLOCK_ID"]) ?: PRODUCTS_IBLOCK_ID;
$arParams["COMPANY_IBLOCK_ID"] = intval($arParams["COMPANY_IBLOCK_ID"]) ?: COMPANY_IBLOCK_ID;
$arParams["LINK_PROPERTY_CODE"] = $arParams["LINK_PROPERTY_CODE"] ?: "COMPANY";
$arParams["DETAIL_URL_TEMPLATE"] = $arParams["DETAIL_URL_TEMPLATE"] ?: "/products/#SECTION_ID#/#ID#/";

if ($this->StartResultCache()) {

    if (!Loader::includeModule("iblock")) {
        $this->abortResultCache();
        return;
    }

    $companyToProduct = [];
    $rs = CIBlockElement::GetList(
        [
            "name" => "asc",
            "sort" => "asc",
        ],
        [
            "IBLOCK_ID" => PRODUCTS_IBLOCK_ID,
            "CHECK_PERMISSIONS" => "Y",
            "!PROPERTY_COMPANY" => false,
            "ACTIVE" => "Y",
        ],
        false,
        false,
        [
            "SORT",
            "NAME",
            "PROPERTY_PRICE",
            "PROPERTY_MATERIAL",
            "PROPERTY_{$arParams["LINK_PROPERTY_CODE"]}",
            "DETAIL_PAGE_URL",
            "ID",
        ]
    );

    if ($arParams["DETAIL_URL_TEMPLATE"]) {
        $rs->SetUrlTemplates($arParams["DETAIL_URL_TEMPLATE"]);
    }

    while ($item = $rs->GetNext(false, false)) {
        foreach ($item["PROPERTY_{$arParams["LINK_PROPERTY_CODE"]}_VALUE"] as $companyId) {
            $companyToProduct[$companyId][] = [
                "NAME" => $item["NAME"],
                "PRICE" => $item["PROPERTY_PRICE_VALUE"],
                "MATERIAL" => $item["PROPERTY_MATERIAL_VALUE"],
                "DETAIL_PAGE_URL" => $item["DETAIL_PAGE_URL"],
            ];
        }
    }

    $companyId = array_keys($companyToProduct);

    $arResult = [
        "ITEMS" => [],
        "COUNT" => 0,
    ];
    if ($companyId) {
        $rs = CIBlockElement::GetList(
            [],
            [
                "IBLOCK_ID" => COMPANY_IBLOCK_ID,
                "CHECK_PERMISSIONS" => "Y",
                "ACTIVE" => "Y",
                "ID" => $companyId,
            ],
            false,
            false,
            [
                "NAME",
                "ID"
            ]
        );

        while ($item = $rs->GetNext(false, false)) {
            if ($products = $companyToProduct[$item["ID"]]) {
                $arResult["ITEMS"][] = [
                    "NAME" => $item["NAME"],
                    "PRODUCTS" => $products,
                ];
            } else {
                unset($companyToProduct[$item["ID"]]);
            }
        }

        $arResult["COUNT"] = count($arResult["ITEMS"]);
    }

    $this->setResultCacheKeys(["COUNT"]);

    $this->includeComponentTemplate();

}

$APPLICATION->SetTitle("Разделов: {$arResult["COUNT"]}");
