<?php
global $USER, $adminMenu;
//-пользователь принадлежит группе "Контент-редактор"
//-пользователю закрыт доступ к главному модулю
if (in_array(groupCodes()[CONTENT_EDITOR_GROUP], $USER->GetUserGroup($USER->GetID())) && CMain::GetUserRight("main") == "D") {
    $items = [];
    foreach ($adminMenu->aGlobalMenu["global_menu_content"]["items"] as $item) {
        if ($item["icon"] == "iblock_menu_icon_types") {
            $items[] = $item;
        }
    }

    if ($items) {
        //скрыть "Рабочий стол"
        unset($adminMenu->aGlobalMenu["global_menu_desktop"]);

        $adminMenu->aGlobalMenu["global_menu_content"]["items"] = $items;
    }
}
