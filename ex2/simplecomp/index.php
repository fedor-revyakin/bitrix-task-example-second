<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

global $APPLICATION;

$APPLICATION->IncludeComponent("simplecomp.exam", ".default",
    [
        "PRODUCTS_IBLOCK_ID" => "2",
        "NEWS_IBLOCK_ID" => "1",
        "LINK_PROPERTY_CODE" => "UF_NEWS_LINK",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36000000"
    ],
);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");