<?php
if (! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

/** @var $arResult */
?>

<?if (! $arResult["ITEMS"]):?>
    <h3>
        Нет новостей, связанных с авторами
    </h3>
<?else:?>
    <div>
        <ul>
            <?foreach ($arResult["ITEMS"] as $item):?>
                <li>
                    <?="[{$item["AUTHOR_ID"]}] - {$item["AUTHOR_LOGIN"]}"?>
                    <ul>
                        <?foreach ($item["NEWS"] as $news):?>
                            <li>
                                <?="{$news["TITLE"]} - {$news["ACTIVE_FROM"]}"?>
                            </li>
                        <?endforeach;?>
                    </ul>
                </li>
            <?endforeach;?>
        </ul>
    </div>
<?endif;?>