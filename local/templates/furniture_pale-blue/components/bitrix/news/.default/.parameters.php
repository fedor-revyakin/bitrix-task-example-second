<?php
if (! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

$arTemplateParameters = [
    "SET_SPECIAL_DATE" => [
        "NAME" => "Установить свойство страницы specialdate",
        "TYPE" => "CHECKBOX",
        "MULTIPLE" => "N",
        "VALUE" => "Y",
        "DEFAULT" => "Y",
    ],
    "CANONICAL_IBLOCK_ID" => [
        "NAME" => "ID информационного блока для rel=canonical",
        "TYPE" => "STRING",
        "DEFAULT" => CANONICAL_IBLOCK_ID,
    ],
];
