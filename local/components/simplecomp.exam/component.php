<?php
if (! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @var CBitrixComponent $this */

use Bitrix\Main\Loader;

if (! isset($arParams["CACHE_TIME"])) {
    $arParams["CACHE_TIME"] = 36000000;
}

$additionalFilter = [];
if (isset($_REQUEST["F"])) {
    $arParams["CACHE_TIME"] = 0;
    $additionalFilter[] = [
        "LOGIC" => "OR",
        [
            "LOGIC" => "AND",
            "<=PROPERTY_PRICE" => "1700",
            "PROPERTY_MATERIAL" => "Дерево, ткань",
        ],
        [
            "LOGIC" => "AND",
            "<PROPERTY_PRICE" => "1500",
            "PROPERTY_MATERIAL" => "Металл, пластик",
        ]
    ];
}

$arParams["PRODUCTS_IBLOCK_ID"] = intval($arParams["PRODUCTS_IBLOCK_ID"]) ?: PRODUCTS_IBLOCK_ID;
$arParams["NEWS_IBLOCK_ID"] = intval($arParams["NEWS_IBLOCK_ID"]) ?: NEWS_IBLOCK_ID;
$arParams["LINK_PROPERTY_CODE"] = $arParams["LINK_PROPERTY_CODE"] ?: "UF_NEWS_LINK";

$newsToSection = [];
$filterSection = [];

if ($this->StartResultCache()) {

    if(! Loader::includeModule("iblock")) {
        $this->abortResultCache();
        return;
    }

    $rs = CIBlockSection::GetList(
        [],
        [
            "IBLOCK_ID" => PRODUCTS_IBLOCK_ID,
            "!UF_NEWS_LINK" => false,
            "ACTIVE" => "Y",
        ],
        false,
        [
            "ID",
            "NAME",
            "UF_NEWS_LINK",
        ]
    );

    while ($item = $rs->GetNext(false, false)) {
        foreach ($item["UF_NEWS_LINK"] as $link) {
            $newsToSection[$link][] = $item["ID"];
            $arResult["ITEMS"][$link]["SECTION_NAME"] .= $arResult["ITEMS"][$link]["SECTION_NAME"] ? ", {$item["NAME"]}" : "{$item["NAME"]}";
        }
    }

    if ($newsToSection) {
        $rs = CIBlockElement::GetList(
            [],
            [
                "IBLOCK_ID" => NEWS_IBLOCK_ID,
                "ID" => array_keys($newsToSection),
                "ACTIVE" => "Y",
            ],
            false,
            false,
            [
                "ID",
                "NAME",
                "ACTIVE_FROM",
            ]
        );

        while ($item = $rs->GetNext(false, false)) {
            $arResult["ITEMS"][$item["ID"]]["NEWS"] = [
                "NAME" => $item["NAME"],
                "DATE" => $item["ACTIVE_FROM"],
            ];
        }

        //удалить неактивные новости, полученные при запросе разделов и связанных с ними id новостей
        foreach ($arResult["ITEMS"] as $newsId => $value) {
            if (! $value["NEWS"]) {
                unset($arResult["ITEMS"][$newsId], $newsToSection[$newsId]);
            } else {
                $filterSection = array_merge($filterSection, $newsToSection[$newsId]);
            }
        }
    }

    $filter = array_merge(
        [
            "IBLOCK_ID" => PRODUCTS_IBLOCK_ID,
            "IBLOCK_SECTION_ID" => $filterSection,
            "ACTIVE" => "Y",
        ],
        $additionalFilter
    );

    $count = 0;
    if ($filterSection) {
        $rs = CIBlockElement::GetList(
            [],
            $filter,
            false,
            false,
            [
                "ID",
                "NAME",
                "IBLOCK_SECTION_ID",
                "PROPERTY_PRICE",
                "PROPERTY_MATERIAL",
                "PROPERTY_ARTNUMBER",
            ]
        );

        $count = $rs->SelectedRowsCount();
        $arResult["PRODUCT_ID"] = [];
        while ($item = $rs->GetNext(false, false)) {
            $arResult["PRODUCT_ID"][] = $item["ID"];
            $sectionToProducts[$item["IBLOCK_SECTION_ID"]][] = [
                "ID" => $item["ID"],
                "NAME" => $item["NAME"],
                "PRICE" => $item["PROPERTY_PRICE_VALUE"],
                "MATERIAL" => $item["PROPERTY_MATERIAL_VALUE"],
                "ARTICLE" => $item["PROPERTY_ARTNUMBER_VALUE"],
            ];
        }
    }

    $arResult["COUNT"] = $count;
    foreach ($newsToSection as $newsId => $sections) {
        foreach ($sections as $sectionId) {
            $arResult["ITEMS"][$newsId]["PRODUCTS"] = array_merge($arResult["ITEMS"][$newsId]["PRODUCTS"] ?? [], $sectionToProducts[$sectionId] ?? []);
        }
        if (! $arResult["ITEMS"][$newsId]["PRODUCTS"]) {
            unset($arResult["ITEMS"][$newsId]);
        }
    }

    $this->setResultCacheKeys(["COUNT", "PRODUCT_ID"]);

    $this->includeComponentTemplate();
}
