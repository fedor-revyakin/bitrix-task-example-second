<?php
if (! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arCurrentValues */

use Bitrix\Main\Loader;

if (! Loader::includeModule('iblock')) {
    return;
}

$iblockExists = (!empty($arCurrentValues['IBLOCK_ID']) && (int)$arCurrentValues['IBLOCK_ID'] > 0);

$db_iblock = CIBlock::GetList(["SORT" => "ASC"], ["ACTIVE" => "Y"]);
while($arRes = $db_iblock->Fetch()) {
    $arIBlocks[$arRes["ID"]] = "[" . $arRes["ID"] . "] " . $arRes["NAME"];
}

$arComponentParameters = [
    "PARAMETERS" => [
        "NEWS_IBLOCK_ID" => [
            "PARENT" => "BASE",
            "NAME" => "ID инфоблока новостей",
            "TYPE" => "LIST",
            "VALUES" => $arIBlocks,
        ],
        "LINK_PROPERTY_CODE" => [
            "PARENT" => "BASE",
            "NAME" => "Код свойства привязки к авторам",
            "TYPE" => "STRING",
            "DEFAULT" => "AUTHOR",
        ],
        "UF_AUTHOR_CODE" => [
            "PARENT" => "BASE",
            "NAME" => "Код пользовательского свойства Тип автора",
            "TYPE" => "STRING",
        ],
        "CACHE_TIME"  =>  [
            "DEFAULT" => 36000000
        ],
    ],
];
