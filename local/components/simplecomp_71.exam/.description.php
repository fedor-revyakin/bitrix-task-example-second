<?php
if (! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}

$arComponentDescription = [
    "NAME" => "Мой компонент (задача 71)",
    "PATH" => [
        "ID" => "exam",
        "NAME" => "Экзамен 2"
    ],
];
