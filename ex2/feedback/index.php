<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

global $APPLICATION;

$APPLICATION->IncludeComponent("bitrix:main.feedback", "",
    [
        "EVENT_MESSAGE_ID" => [FEEDBACK_FORM_EVENT_TEMPLATE],
        "OK_TEXT" => "Сообщение отправлено",
        "REQUIRED_FIELDS" => [
            "NAME",
            "EMAIL",
            "MESSAGE"
        ],
        "USE_CAPTCHA" => "N",
    ]
);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");