<?php
if (! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

/** @var $this CBitrixComponent */
/** @var $USER CUser */
/** @var $arParams */
/** @var $arResult */
/** @var $APPLICATION */


use Bitrix\Main\Loader;

if (! $USER->IsAuthorized()) {
    LocalRedirect("/login/");
}

if (! isset($arParams["CACHE_TIME"])) {
    $arParams["CACHE_TIME"] = 36000000;
}

$arParams["NEWS_IBLOCK_ID"] = intval($arParams["NEWS_IBLOCK_ID"]) ?: NEWS_IBLOCK_ID;
$arParams["LINK_PROPERTY_CODE"] = strval($arParams["LINK_PROPERTY_CODE"]) ?: "AUTHOR";
$arParams["UF_AUTHOR_CODE"] = strval($arParams["UF_AUTHOR_CODE"]) ?: "UF_AUTHOR_TYPE";

if ($this->StartResultCache(false, $USER->GetID())) {
    if(! Loader::includeModule("iblock")) {
        $this->AbortResultCache();
        ShowError("Не установлен модуль инфоблоки");
        return;
    }

    $myAuthors = [];
    $rsUser = CUser::GetByID($USER->GetID());
    $myGroup = $rsUser->Fetch()[$arParams["UF_AUTHOR_CODE"]];

    $rs = CUser::GetList("", "",
        [
            $arParams["UF_AUTHOR_CODE"] => $myGroup,
            "ACTIVE" => "Y",
        ],
        [
            "FIELDS" => ["ID", "LOGIN"],
        ]
    );

    while ($user = $rs->GetNext()) {
        $myAuthors[$user["ID"]] = $user["LOGIN"];
    }
    unset($myAuthors[$USER->GetID()]);

//Получить новости
    $arResult = [
        "ITEMS" => [],
        "COUNT" => 0,
    ];
    $authorProp = "PROPERTY_" . $arParams["LINK_PROPERTY_CODE"];
    $allNews = [];
    if ($myAuthors) {
        $rs = CIBlockElement::GetList(
            [],
            [
                "IBLOCK_ID" => $arParams["NEWS_IBLOCK_ID"],
                "ACTIVE" => "Y",
                [
                    "LOGIC" => "AND",
                    ["ID" => CIBlockElement::SubQuery("ID", ["IBLOCK_ID" => $arParams["NEWS_IBLOCK_ID"], $authorProp => array_keys($myAuthors)])],
                    ["!ID" => CIBlockElement::SubQuery("ID", ["IBLOCK_ID" => $arParams["NEWS_IBLOCK_ID"], $authorProp => $USER->GetID()])],
                ]
            ],
            false,
            false,
            [
                "NAME",
                "ACTIVE_FROM",
                $authorProp,
                "ID",
            ]
        );

        $prevId = null;
        while ($item = $rs->GetNext()) {
            if ($prevId !== $item["ID"]) {
                $arResult["COUNT"]++;
                $prevId = $item["ID"];
            }
            $authorId = $item["{$authorProp}_VALUE"];
            if (! $myAuthors[$authorId]) {
                continue;
            }

            $news = [
                "TITLE" => $item["NAME"],
                "ACTIVE_FROM" => $item["ACTIVE_FROM"],
            ];
            if (! $arResult["ITEMS"][$authorId]) {
                $arResult["ITEMS"][$authorId] = [
                    "AUTHOR_LOGIN" => $myAuthors[$authorId],
                    "AUTHOR_ID" => $authorId,
                    "NEWS" => [$news],
                ];
            } else {
                $arResult["ITEMS"][$authorId]["NEWS"][] = $news;
            }
        }
    }

    $this->SetResultCacheKeys(["COUNT"]);

    $this->IncludeComponentTemplate();
}

$APPLICATION->SetTitle("Новостей: {$arResult["COUNT"]}");
