<?php
if (! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

/** @var array $arResult */
/** @var array $arParams */
/** @global CMain $APPLICATION */
/** @var CBitrixComponent $this */

$APPLICATION->SetTitle("В каталоге товаров представлено товаров: {$arResult["COUNT"]}");

if ($APPLICATION->GetShowIncludeAreas()) {
    $id = $arResult["PRODUCT_ID"][0] ?: 0;

    $button = CIBlock::GetPanelButtons(
        $arParams["PRODUCTS_IBLOCK_ID"],
        $id,
        0,
        ["SECTION_BUTTONS" => false, "SESSID" => false]
    );

    //кнопка Добавить
    $this->AddEditAction($arParams["PRODUCTS_IBLOCK_ID"], $button["edit"]["add_element"]["ACTION_URL"], $button["edit"]["add_element"]["TEXT"]);

    //кнопки Редактировать, Удалить
    foreach ($arResult["PRODUCT_ID"] as $id) {
        $edit = preg_replace("/(\W)ID=\d+/","$1ID={$id}", $button["edit"]["edit_element"]["ACTION_URL"] ?? "");
        $delete = preg_replace("/(\W)ID=\d+/","$1ID={$id}", $button["edit"]["delete_element"]["ACTION_URL"] ?? "");
        $this->AddEditAction($id, $edit);
        $this->AddDeleteAction($id, $delete);
    }

    //Добавить пункт ИБ в админке в выпадающем меню компонента
    $this->AddIncludeAreaIcons(
        [
            [
                "ID" => "admin_ib",
                "TITLE" => "ИБ в админке",
                "URL" => "/bitrix/admin/iblock_element_admin.php?IBLOCK_ID={$arParams["PRODUCTS_IBLOCK_ID"]}&type=products",
                "IN_PARAMS_MENU" => true,
            ],
        ]
    );
}
