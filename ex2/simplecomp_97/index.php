<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

global $APPLICATION;

$APPLICATION->IncludeComponent("simplecomp_97.exam", ".default",
    [
        "NEWS_IBLOCK_ID" => NEWS_IBLOCK_ID,
        "LINK_PROPERTY_CODE" => "AUTHOR",
        "UF_AUTHOR_CODE" => "UF_AUTHOR_TYPE",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36000000",
    ],
);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");