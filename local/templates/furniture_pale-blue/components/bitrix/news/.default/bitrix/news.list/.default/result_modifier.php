<?php
if (! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

/** @var $APPLICATION */
/** @var $arParams */
/** @var $arResult */

$specialDate = $arParams["SET_SPECIAL_DATE"] === "Y" && ($date = $arResult["ITEMS"][0]["DISPLAY_ACTIVE_FROM"]) ?
    $date :
    $APPLICATION->GetProperty(SPECIAL_DATE_PROPERTY_NAME);

$APPLICATION->SetDirProperty(SPECIAL_DATE_PROPERTY_NAME, $specialDate);
